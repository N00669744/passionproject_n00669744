namespace FriendCodes.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public partial class AddActive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Activity", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.Users", "Activity");
        }
    }
    public partial class AddUnitName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Available_Unit", "Unit_Name", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.Available_Unit", "Unit_Name");
        }
    }

    internal sealed class Configuration : DbMigrationsConfiguration<FriendCodes.Models.FriendCodeContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "FriendCodes.Models.FriendCodeContext";
        }

        protected override void Seed(FriendCodes.Models.FriendCodeContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
