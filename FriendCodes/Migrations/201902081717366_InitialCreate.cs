namespace FriendCodes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Available_Unit",
                c => new
                    {
                        AvailableUnit_ID = c.Int(nullable: false, identity: true),
                        Rarity = c.Int(nullable: false),
                        Build = c.String(maxLength: 255),
                        Unit_ID = c.Int(nullable: true),
                        User_User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.AvailableUnit_ID)
                .ForeignKey("dbo.Users", t => t.User_User_ID)
                .Index(t => t.User_User_ID);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        Unit_ID = c.Int(nullable: false, identity: true),
                        Unit_Name = c.String(nullable: false, maxLength: 255),
                        Rarity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Unit_ID);
            
            CreateTable(
                "dbo.Desired_Unit",
                c => new
                    {
                        DesiredUnit_ID = c.Int(nullable: false, identity: true),
                        Rarity = c.Int(nullable: false),
                        User_IGN = c.String(nullable: false, maxLength: 255),
                        Unit_ID = c.Int(nullable: false),
                        User_User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.DesiredUnit_ID)
                .ForeignKey("dbo.Users", t => t.User_User_ID)
                .Index(t => t.User_User_ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        User_ID = c.Int(nullable: false, identity: true),
                        User_IGN = c.String(nullable: false, maxLength: 255),
                        Friend_Code = c.String(nullable: false, maxLength: 9),
                    })
                .PrimaryKey(t => t.User_ID);
            
            CreateTable(
                "dbo.UnitAvailable_Unit",
                c => new
                    {
                        Unit_Unit_ID = c.Int(nullable: false),
                        Available_Unit_AvailableUnit_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Unit_Unit_ID, t.Available_Unit_AvailableUnit_ID })
                .ForeignKey("dbo.Units", t => t.Unit_Unit_ID, cascadeDelete: true)
                .ForeignKey("dbo.Available_Unit", t => t.Available_Unit_AvailableUnit_ID, cascadeDelete: true)
                .Index(t => t.Unit_Unit_ID)
                .Index(t => t.Available_Unit_AvailableUnit_ID);
            
            CreateTable(
                "dbo.Desired_UnitUnit",
                c => new
                    {
                        Desired_Unit_DesiredUnit_ID = c.Int(nullable: false),
                        Unit_Unit_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Desired_Unit_DesiredUnit_ID, t.Unit_Unit_ID })
                .ForeignKey("dbo.Desired_Unit", t => t.Desired_Unit_DesiredUnit_ID, cascadeDelete: true)
                .ForeignKey("dbo.Units", t => t.Unit_Unit_ID, cascadeDelete: true)
                .Index(t => t.Desired_Unit_DesiredUnit_ID)
                .Index(t => t.Unit_Unit_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Desired_Unit", "User_User_ID", "dbo.Users");
            DropForeignKey("dbo.Available_Unit", "User_User_ID", "dbo.Users");
            DropForeignKey("dbo.Desired_UnitUnit", "Unit_Unit_ID", "dbo.Units");
            DropForeignKey("dbo.Desired_UnitUnit", "Desired_Unit_DesiredUnit_ID", "dbo.Desired_Unit");
            DropForeignKey("dbo.UnitAvailable_Unit", "Available_Unit_AvailableUnit_ID", "dbo.Available_Unit");
            DropForeignKey("dbo.UnitAvailable_Unit", "Unit_Unit_ID", "dbo.Units");
            DropIndex("dbo.Desired_UnitUnit", new[] { "Unit_Unit_ID" });
            DropIndex("dbo.Desired_UnitUnit", new[] { "Desired_Unit_DesiredUnit_ID" });
            DropIndex("dbo.UnitAvailable_Unit", new[] { "Available_Unit_AvailableUnit_ID" });
            DropIndex("dbo.UnitAvailable_Unit", new[] { "Unit_Unit_ID" });
            DropIndex("dbo.Desired_Unit", new[] { "User_User_ID" });
            DropIndex("dbo.Available_Unit", new[] { "User_User_ID" });
            DropTable("dbo.Desired_UnitUnit");
            DropTable("dbo.UnitAvailable_Unit");
            DropTable("dbo.Users");
            DropTable("dbo.Desired_Unit");
            DropTable("dbo.Units");
            DropTable("dbo.Available_Unit");
        }
    }
}
