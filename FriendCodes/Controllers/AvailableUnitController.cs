﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FriendCodes.Models;
using System.Data.SqlClient;
using FriendCodes.Models.ViewModels;
using System.Net;

namespace FriendCodes.Controllers
{
    public class AvailableUnitController : Controller
    {
        private FriendCodeContext db = new FriendCodeContext();

        // GET: AvailableUnit
        public ActionResult Index()
        {
            return View(db.Available_Units.ToList());
        }
        //Redirects to the User Controller, UserInfo Action of the specific User ID
        //User/UserInfo/ID 
        public ActionResult UserInfo(int? id)
        {
            if ((id == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if(user == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("UserInfo", "User",new {ID=id});
        }
        // /AvailableUnit/Add
        public ActionResult Add()
        {
            ViewUnits viewunits = new ViewUnits();
            viewunits.unit = db.Units.ToList();
            viewunits.users = db.Users.ToList();

            return View(viewunits);
        }
        // /AvailableUnit/Add/ID
        [HttpPost]
        public ActionResult AddUnit(string A_Unit_Name, int A_Rarity ,string A_Build, int A_User_ID)
        {
            string query = "INSERT INTO Available_Unit (Unit_Name, Rarity, Build, User_User_ID) values (@unit_Name, @avail_rarity, @avail_build, @user_ID)";

            SqlParameter[] unitParams = new SqlParameter[4];
            unitParams[0] = new SqlParameter("@unit_Name", A_Unit_Name);
            unitParams[1] = new SqlParameter("@avail_rarity",A_Rarity);
            unitParams[2] = new SqlParameter("@avail_build",A_Build);
            unitParams[3] = new SqlParameter("@user_ID", A_User_ID);

            db.Database.ExecuteSqlCommand(query, unitParams);
            return RedirectToAction("Index");
        }

        // /AvailableUnit/Edit/
        /*Retrieves the desired unit from the Available_Units DB and 
         * returns in the AvailableUnit view to be displayed
         */
        public ActionResult Edit(int id)
        {
            EditUnit editunits = new EditUnit();
            editunits.available_unit = db.Available_Units.Find(id);
            editunits.users = db.Users.ToList();

            return View(editunits);
        }

        // /AvailableUnit/Edit/ID
        /*Upon submission, any changes to the Unit are updated in the DB*/
        [HttpPost]
        public ActionResult Edit(int id, string A_Unit_Name, int A_Rarity, string A_Build, int A_User_ID)
        {
            if ((id == null) || (db.Available_Units.Find(id) == null))
            {
                return HttpNotFound();
            }

            string query = "UPDATE Available_Unit set Unit_Name=@unit_Name, Rarity=@avail_rarity, Build=@avail_build where AvailableUnit_ID=@avail_ID";
            SqlParameter[] unitParams = new SqlParameter[5];
            unitParams[0] = new SqlParameter("@unit_Name", A_Unit_Name);
            unitParams[1] = new SqlParameter("@avail_rarity", A_Rarity);
            unitParams[2] = new SqlParameter("@avail_build", A_Build);
            unitParams[3] = new SqlParameter("@user_ID", A_User_ID);
            unitParams[4] = new SqlParameter("@avail_ID", id);

            db.Database.ExecuteSqlCommand(query, unitParams);

            return RedirectToAction("Index");
        }
        //AvailableUnit/Delete/ID
        public ActionResult Delete(int? id)
        {
            if(id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Available_Unit available_Unit = db.Available_Units.Find(id);
            if (available_Unit == null)
            {
                return HttpNotFound();
            }
            return View(available_Unit);
        }
        /*Delete Available_Unit from the DB */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteUnit(int id)
        {
            //remove associated units from that user
            string query = "DELETE FROM Available_Unit where AvailableUnit_ID=@id";
            SqlParameter[] unitparam = new SqlParameter[1];
            unitparam[0] = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, unitparam);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        //Dispose of extra information related to the current instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}