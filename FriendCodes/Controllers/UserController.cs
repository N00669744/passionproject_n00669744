﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FriendCodes.Models;

/* Used as sorting/search reference: https://docs.microsoft.com/en-us/aspnet/mvc/overview/getting-started/getting-started-with-ef-using-mvc/sorting-filtering-and-paging-with-the-entity-framework-in-an-asp-net-mvc-application*/
/*Christine Bittle http5204-FirstMVC used as reference*/
namespace FriendCodes.Controllers
{
    public class UserController : Controller
    {
        private FriendCodeContext db = new FriendCodeContext();

        /* Attempt to add a Search Bar for Units
                public ViewResult UserSearch(string userSearch)
                {

                    var users = from u in db.Users select u;

                    if (!String.IsNullOrEmpty(userSearch))
                    {
                        users = users.Where(u=>u.User_IGN.Contains(userSearch)||u.Friend_Code.Contains(userSearch));
                    }

                    return View(db.Users.ToList());
                }
          */

         //Displays all the Users in the DB
        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        //User/UserInfo/1
        /*Finds the desired User Information to display based on the User_ID*/
        public ActionResult UserInfo(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if(user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        public ActionResult Create()
        {
            return View();
        }

        //Creates a new User in Users
        //User/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "User_ID,User_IGN,Friend_Code,UserActive")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        //Checks for the valid ID to be editted
        //User/Edit/1
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //Edits Users Info if any chnages are made 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "User_ID,User_IGN,Friend_Code,UserActive")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // DELETE: Check for valid ID, then finds the desired User 
        public ActionResult Delete (int? id)
        {
            if (id == null){
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null) {
                return HttpNotFound();
            }
            return View(user);
        }

        //Deletes Users Info upon confirmation
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteUser(int id)
        {
            //remove associated available units from that user
            string query = "DELETE FROM Available_Unit where User_User_ID=@id";
            SqlParameter unitparam = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, unitparam);

            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();

           return RedirectToAction("Index");
        }
        //Dispose of extra information related to the current instance
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}