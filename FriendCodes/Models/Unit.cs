﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace FriendCodes.Models
{
    public class Unit
    {
        [Key]
        public int Unit_ID { get; set; }

        [Required,StringLength(255),Display(Name = "Unit Name")]
        public string Unit_Name { get; set; }

        [Required]
        public int Rarity { get; set; }

        public virtual ICollection<Available_Unit> Available_Units { get; set; }
        public virtual ICollection<Desired_Unit> Desired_Units { get; set; }
    }
}