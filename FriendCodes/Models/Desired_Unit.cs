﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;


/***Looking for:**  6★ Randi
    Specs: With Fixed Dice
*/
namespace FriendCodes.Models
{
    public class Desired_Unit
    {
        //ScaffoldColumn set to false hides the ID from being displayed in the View
        [Key,ScaffoldColumn(false)]
        public int DesiredUnit_ID { get; set; }

        [Required, Range(6, 7), Display(Name = "Rarity")]
        public int Rarity { get; set; }

        [Required, StringLength(255), Display(Name = "Specifications")]
        public string User_IGN { get; set; }

        [Required]
        public int Unit_ID { get; set; }

        [Required, StringLength(255), Display(Name = "Unit Name")]
        public string Unit_Name { get; set; }

        //Units Owner
        public virtual User User { get; set; }
        public virtual ICollection<Unit> Units { get; set; }
    }
}