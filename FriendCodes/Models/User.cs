﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;


/***Friend Code:** 111,111,111
* **IGN:** John Doe.*/
namespace FriendCodes.Models
{
    public enum Activity
    {
        Daily,
        Occasional,
        Rarely
    }

    public class User
    {
        [Key]
        public int User_ID { get; set; }

        [Required(ErrorMessage ="Please Enter your In Game Name"), StringLength(255),Display(Name ="User IGN: ")]
        public string User_IGN { get; set; }

        [Required(ErrorMessage = "Please Enter your Friend Code"), StringLength(9),Display(Name ="Friend Code: "),DisplayFormat()]
        public string Friend_Code { get; set; }

        [Required(ErrorMessage = "Please choose an option"),Display(Name="Activity: ")]
        public Activity UserActive { get; set; }

        public virtual ICollection<Available_Unit> Available_Units { get; set; }
        public virtual ICollection<Desired_Unit> Desired_Units { get; set; }
    }
}