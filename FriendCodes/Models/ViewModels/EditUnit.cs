﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FriendCodes.Models.ViewModels
{
    public class EditUnit
    {
        public EditUnit()
        {

        }
  
        public virtual Available_Unit available_unit { get; set; }

        public IEnumerable<User> users { get; set; }
        
    }

}